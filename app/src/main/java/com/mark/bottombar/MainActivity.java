package com.mark.bottombar;

import android.support.annotation.NonNull;

import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    RelativeLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = (RelativeLayout) findViewById(R.id.container);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_new:
                        openGragmentA(1);
                        break;
                    case R.id.action_edit:
                        openGragmentA(2);
                        break;
                    case R.id.action_about:
                        openGragmentA(3);
                        break;
                }
                return true;
            }

            private void openGragmentA(int id) {
                AFragment fragmentA = new AFragment();
                BFragment fragmentB = new BFragment();
                CFragment fragmentC = new CFragment();
                FragmentManager manager  = getSupportFragmentManager();
                switch (id){
                    case 1:
                        manager.beginTransaction().replace(R.id.container,fragmentA).commit();
                        break;
                    case  2:
                        manager.beginTransaction().replace(R.id.container,fragmentB).commit();
                        break;
                    case  3:
                        manager.beginTransaction().replace(R.id.container,fragmentC).commit();
                        break;

                }

            }


            private void messageBox(String s) {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
